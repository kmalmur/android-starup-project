package com.kmsoftware.training01.sample.presentation;

import com.kmsoftware.training01.sample.presentation.internal.di.components.ApplicationComponent;
import com.kmsoftware.training01.sample.presentation.internal.di.components.DaggerApplicationComponent;
import com.kmsoftware.training01.sample.presentation.internal.di.modules.ApplicationModule;

import android.app.Application;

/**
 * Android Main Application
 */
public class AndroidApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
