/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kmsoftware.training01.sample.presentation.internal.di.modules;

import android.content.Context;

import com.kmsoftware.training01.sample.domain.executor.BackgroundSchedulerProvider;
import com.kmsoftware.training01.sample.domain.executor.UISchedulerProvider;
import com.kmsoftware.training01.sample.presentation.AndroidApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {

    private final AndroidApplication application;

    public ApplicationModule(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    BackgroundSchedulerProvider provideBackgroundSchedulerProvider() {
        return new BackgroundSchedulerProvider() {
            @Override
            public Scheduler getScheduler() {
                return Schedulers.newThread();
            }
        };
    }

    @Provides
    @Singleton
    UISchedulerProvider provideUISchedulerProvider() {
        return new UISchedulerProvider() {
            @Override
            public Scheduler getScheduler() {
                return AndroidSchedulers.mainThread();
            }
        };
    }


//    @Provides
//    @Singleton
//    GetListOfAvailableSharesUseCase provideGetListOfAvailableSharesUseCase(StockManagementRepository repository,
//                                                                           BackgroundSchedulerProvider backgroundScheduler,
//                                                                           UISchedulerProvider uiSchedulerProvider) {
//
//        return new GetListOfAvailableSharesUseCase(repository, backgroundScheduler, uiSchedulerProvider);
//    }

}
