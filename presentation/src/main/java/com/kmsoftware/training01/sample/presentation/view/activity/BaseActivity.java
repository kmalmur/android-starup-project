package com.kmsoftware.training01.sample.presentation.view.activity;

import com.kmsoftware.training01.sample.presentation.AndroidApplication;
import com.kmsoftware.training01.sample.presentation.internal.di.components.ApplicationComponent;

import android.app.Activity;
import android.os.Bundle;

/**
 * Base {@link android.app.Activity} class for every Activity in this application.
 */
public abstract class BaseActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    /**
     * Get the Main Application component for dependency injection.
     *
     * @return {@link ApplicationComponent}
     */
    protected ApplicationComponent getApplicationComponent() {
        return ((AndroidApplication) getApplication()).getApplicationComponent();
    }

}
