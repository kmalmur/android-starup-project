package com.kmsoftware.training01.sample.presentation.internal.di.components;

import com.kmsoftware.training01.sample.presentation.internal.di.modules.ApplicationModule;
import com.kmsoftware.training01.sample.presentation.view.activity.BaseActivity;
import com.kmsoftware.training01.sample.presentation.view.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);

}
