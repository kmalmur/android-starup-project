package com.kmsoftware.training01.sample.domain.interactor;

import com.kmsoftware.training01.sample.domain.executor.BackgroundSchedulerProvider;
import com.kmsoftware.training01.sample.domain.executor.UISchedulerProvider;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 * <p>
 * By convention each UseCase implementation will return the result using a {@link rx.Subscriber}
 * that will execute its job in a background thread and will post the result in the UI thread.
 */
public abstract class UseCase {

    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    private final UISchedulerProvider uiSchedulerProvider;

    private Subscription subscription = Subscriptions.empty();

    protected UseCase(BackgroundSchedulerProvider backgroundSchedulerProvider, UISchedulerProvider uiSchedulerProvider) {
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
        this.uiSchedulerProvider = uiSchedulerProvider;
    }



    /**
     * Builds an {@link rx.Observable} which will be used when executing the current {@link UseCase}.
     */
    protected abstract Observable buildUseCaseObservable();

    /**
     * Executes the current use case.
     *
     * @param subscriber The guy who will be listen to the observable build with {@link #buildUseCaseObservable()}.
     */
    @SuppressWarnings("unchecked")
    public void execute(Subscriber subscriber) {
        this.subscription = this.buildUseCaseObservable()
                .subscribeOn(backgroundSchedulerProvider.getScheduler())
                .observeOn(uiSchedulerProvider.getScheduler())
                .subscribe(subscriber);
    }

    /**
     * Executes the current use case. This implementation is intended to be used in tests, to simplify mocking.
     *
     * @param subscriber The guy who will be listen to the observable build with {@link #buildUseCaseObservable()}.
     */
    @SuppressWarnings("unchecked")
    public void executeUsingDefaultSchedulers(Subscriber subscriber) {
        this.subscription = this.buildUseCaseObservable()
                .subscribe(subscriber);
    }

    /**
     * Unsubscribes from current {@link rx.Subscription}.
     */
    public void unsubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
