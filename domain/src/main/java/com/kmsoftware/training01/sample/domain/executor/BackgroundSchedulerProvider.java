package com.kmsoftware.training01.sample.domain.executor;

import rx.Scheduler;

public interface BackgroundSchedulerProvider {

    Scheduler getScheduler();
}
